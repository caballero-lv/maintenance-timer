import moment from "moment";

(function ($) {
  const M_Timer = {
    index_php: {
      init: function () {
        let eventTime = moment(
          cab_m_timer.date + " 12:00:00",
          "DD/MM/YYYY HH:mm:ss"
        );

        let currentTime = moment();
        let diffTime = eventTime.unix() - currentTime.unix();
        let duration = moment.duration(diffTime * 1000, "milliseconds");
        const interval = 1000;
        let tick = false;

        if (diffTime > 0) {
          tick = setInterval(function () {
            // Update timer every second
            duration = moment.duration(duration - interval, "milliseconds");
            if (duration.seconds() >= 0) {
              $("#cab-timer-years").text(duration.years());
              $("#cab-timer-months").text(duration.months());
              // $("#cab-timer-weeks").text(duration.weeks());
              $("#cab-timer-days").text(duration.days());
              $("#cab-timer-hours").text(duration.hours());
              $("#cab-timer-minutes").text(duration.minutes());
              $("#cab-timer-seconds").text(duration.seconds());
            } else {
              clearInterval(tick);
            }
          }, interval);
        }

        // Toggle
        function toggle_box(target) {
          $(".cab-maint__box").each(function () {
            var box = $(this);
            if (box.data("container") == target) {
              box.fadeIn();
              box.addClass("cab-maint__box--open");
            } else {
              box.hide();
              box.removeClass("cab-maint__box--open");
            }
          });
        }

        $("body").on("click", ".cab-maint__toggle", function (e) {
          e.preventDefault();

          var btn = $(this);
          var target = btn.attr("data-target");

          toggle_box(target);
        });
      },
    },

    settings_page_m_timer_options: {
      init: function () {
        $(".cab-m-date").datepicker({
          dateFormat: "dd.mm.yy",
        });
      },
    },
  };

  const UTIL = {
    fire: function (func, funcname, args) {
      var fire;
      var namespace = M_Timer;
      funcname = funcname === undefined ? "init" : funcname;
      fire = func !== "";
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === "function";

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function () {
      // Fire common init JS
      UTIL.fire("common");

      // Fire page-specific init JS, and then finalize JS
      $.each(
        document.body.className.replace(/-/g, "_").split(/\s+/),
        function (i, classnm) {
          UTIL.fire(classnm);
          UTIL.fire(classnm, "finalize");
        }
      );

      // Fire common finalize JS
      UTIL.fire("common", "finalize");
    },
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);
})(jQuery);
