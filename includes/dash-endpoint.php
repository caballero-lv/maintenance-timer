<?php
/**
 * Plugin dash
 */

class Cab_M_Timer_Dash
{
    public function __construct()
    {
        add_action('wp_ajax_nopriv_cab_get_maint', array($this, 'get_data'));
        add_action('wp_ajax_nopriv_cab_update_maint', array($this, 'update_data'));
    }


    private function checkToken()
    {
        
        if (! isset($_POST['token']) ) {
            wp_send_json_error('No token');
        }

        // Verify token
        $token = wp_unslash($_POST['token']);

        if ($token !== 'H7HCGsDdYzaQy+tb#!9YH+' ) {
            wp_send_json_error('Invalid token');
        }
    }

    public function get_data()
    {
        $this->checkToken();

        // Get data
        $maint_date = get_option('cab-m-timer-date');
        $maintained = get_option('cab-m-timer-last-date');
        $manager    = get_option('cab-m-timer-pers');

        // Create response array
        $response = array(
			'maint_date'      => $maint_date,
			'maintained_last' => $maintained,
        	'manager'         => $manager
        );

        // Send response
        wp_send_json_success($response);
    }

    public function update_data()
    {
        $this->checkToken();

        $next_update = isset($_POST['next_update']) ? sanitize_text_field($_POST['next_update']) : '';
        $last_update = isset($_POST['last_update']) ? sanitize_text_field($_POST['last_update']) : '';
        $manager = isset($_POST['manager']) ? wp_unslash( (int) $_POST['manager'] ) : '';

        update_option('cab-m-timer-date', $next_update);
        update_option('cab-m-timer-last-date', $last_update);
        update_option('cab-m-timer-pers', $manager);

        wp_send_json_success();
    }
}

new Cab_M_Timer_Dash();
