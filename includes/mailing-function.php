<?php
/**
 * Mailing function
 *
 * @package Mailing
 */

/**
 * Crate Caballero reminder cron
 */
function cab_send_reminder() {
	$subject = sprintf( pllc__( 'Message from %s Maintenance Timer', 'Maintenance Timer' ), get_bloginfo( 'name' ) );
	$headers = 'From: ' . get_bloginfo( 'name' ) . ' Maintenance Timer <dev@caballero.lv>' . "\r\n";
	$to      = '1' === get_option( 'cab-m-timer-pers' ) ? 'info@pragmatik.lv' : 'info@caballero.lv';
	$message = 'The site ' . get_bloginfo( 'name' ) . ' needs to have a Maintenance.';
	$mailed  = wp_mail( $to, $subject, $message, $headers );
	if ( $mailed ) :
		wp_clear_scheduled_hook( 'cab_reminder_action' );
	endif;
}
add_action( 'cab_reminder_action', 'cab_send_reminder' );
