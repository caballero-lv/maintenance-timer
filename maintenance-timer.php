<?php
/**
 * A dashboard widget for reminding site admin when a site update is due.
 *
 * @package Maintenance timer
 * 
 * @wordpress-plugin
 * Plugin Name:       Maintenance timer
 * Version:           2.2.0
 * Author:            Caballero
 * Author URI:        https://www.caballero.lv
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       maintenance-timer
 */

?>
<?php
/**
 * Maintenance timer
 */
class Cab_M_Timer
{
    /**
     * Plugin directory uri
     *
     * @var string
     */
    public $m_uri;

    /**
     * Plugin directory path
     *
     * @var string
     */
    public $m_dir;


    /**
     * Date today
     *
     * @var string
     */
    protected $_today;

    /**
     * Date to count down to
     *
     * @var string
     */
    protected $_date;

    /**
     * Date of last update
     *
     * @var string
     */
    protected $_last_date;

    /**
     * Time difference between today and end date
     *
     * @var string
     */
    protected $_diff;


    /**
     * Color class
     *
     * @var string
     */
    protected $_man;

    /**
     * Array of contact persons
     *
     * @var array
     */
    protected $m_pers;

    /**
     * Color class
     *
     * @var string
     */
    protected $_color_class;

    /**
     * Verify token
     *
     * @var string
     */
    protected $_token;


    /**
     * Contstructs
     */
    public function __construct()
    {

        $this->m_uri = plugin_dir_url(__FILE__);
        $this->m_dir = plugin_dir_path(__FILE__);

        // process all $_GET and $_POST requests.
        add_action('init', array($this, 'cab_m_timer_requests'));

        $this->m_pers     = $this->set_persons();
        $this->_man       = get_option('cab-m-timer-pers');
        $this->_date      = get_option('cab-m-timer-date');
        $this->_last_date = get_option('cab-m-timer-last-date');
        $this->_token       = 'H7HCGsDdYzaQy+tb#!9YH+';

        $this->_today = time();
        $this->calc_date_diff();

        // enqueue js and css.
        add_action('admin_enqueue_scripts', array($this, 'cab_m_timer_scripts'));

        // register plugin options page.
        add_action('admin_menu', array($this, 'cab_m_timer_options'));

        if ($this->_date) :
            // register the dashboard widget.
            add_action('wp_dashboard_setup', array($this, 'cab_register_m_timer'));
        endif;

        $this->disable_wp_updates();
        $this->add_mailing_function();
        $this->dash_endpoint();
    }

    /**
     * Scripts
     */
    public function cab_m_timer_scripts()
    {

        $scr = get_current_screen();

        if (isset($scr->base)) :
            if ('dashboard' == $scr->base || 'settings_page_m-timer-options' == $scr->base) :
                $data = array(
                    'date' => $this->_date,
                );

                wp_register_script('cab-m-timer', $this->m_uri . '/app/js/cab-m-timer.js', [], time(), true);
                wp_localize_script('cab-m-timer', 'cab_m_timer', $data);
                wp_enqueue_script('jquery');
                wp_enqueue_script('cab-m-timer');
            endif;

            if ('dashboard' == $scr->base) :
                wp_enqueue_style('cab-m-timer-css', $this->m_uri . '/app/styles/screen.css', [], time());
            elseif ('settings_page_m-timer-options' == $scr->base) :
                wp_enqueue_style('jquery-ui', '//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css');
                wp_enqueue_script('jquery-ui-datepicker');
            endif;
        endif;
    }


    /**
     * Adds widget
     */
    public function cab_register_m_timer()
    {
        $widget_id   = 'cab-m-timer-widget';
        $widget_name = __('Mājas lapas apkope');
        $callback    = array($this, 'cab_m_timer_content');
        wp_add_dashboard_widget($widget_id, $widget_name, $callback);
    }


    /**
     * Content
     */
    public function cab_m_timer_content()
    {
        include $this->m_dir . 'views/widget/body.php';
    }


    /**
     * Adds option page
     */
    public function cab_m_timer_options()
    {
        add_options_page(
            'Maintenance timer settings',
            'Maintenance timer',
            'manage_options',
            'm-timer-options',
            array($this, 'cab_b_timer_options_content')
        );
    }


    /**
     * Content
     */
    public function cab_b_timer_options_content()
    {
        include $this->m_dir . 'views/options/body.php';
    }


    /**
     * Request
     */
    public function cab_m_timer_requests()
    {

        if (isset($_POST['cab-m-timer-date']) && wp_verify_nonce(wp_unslash($_POST['cab_m_timer_nonce']), 'cab-save-m-timer-options')) :

            $this->_date      = wp_strip_all_tags(wp_unslash($_POST['cab-m-timer-date']));
            $this->_last_date = wp_strip_all_tags(wp_unslash($_POST['cab-m-timer-date-last']));
            $this->_man       = wp_strip_all_tags(wp_unslash($_POST['cab-m-timer-pers']));

            update_option('cab-m-timer-date', $this->_date);
            update_option('cab-m-timer-last-date', $this->_last_date);
            update_option('cab-m-timer-pers', $this->_man);

            $this->create_reminder();

            define('CAB_M_TIMER_SAVED', true);
        endif;
    }


    /**
     * Sets persons
     */
    protected function set_persons()
    {
        return array(
            '1' => array(
                'name'     => 'Pragmatik',
                'lastname' => '',
                'prefix'   => '',
                'phone'    => '+371 25 377 773',
                'email'    => 'info@pragmatik.lv',
                'image'    => '',
            ),
            '2' => array(
                'name'     => 'Caballero',
                'lastname' => '',
                'prefix'   => '',
                'phone'    => '+371 20 400 222',
                'email'    => 'info@caballero.lv',
                'image'    => '',
            ),
        );
    }


    /**
     * Returns person
     */
    public function get_persons()
    {
        return $this->m_pers;
    }


    /**
     * Calcculates date
     */
    protected function calc_date_diff()
    {

        if (!$this->_today || !$this->_date) :
            return array(
                'years'   => '',
                'months'  => '',
                'weeks'   => '',
                'days'    => '',
                'hours'   => '',
                'minutes' => '',
                'secs'    => '',
            );
        endif;

        $def_today = new DateTime('@' . $this->_today);
        $def_end = new DateTime('@' . $this->_today);
        $today = $def_today;
        $end   = $def_end;

        // Years
        $year_diff = $today->diff($end);
        $years = $year_diff->y;
        // Months
        $end = $end->modify("-$years years");
        $m_diff = $today->diff($end);
        $months = $m_diff->m;
        // Weeks
        $end = $end->modify("-$months months");
        $w_diff = $today->diff($end);
        $weeks = $w_diff->d / 7;
        // Days
        $end = $end->modify("-$weeks weeks");
        $d_diff = $today->diff($end);
        $days = $d_diff->d;
        // Hours
        $end = $end->modify("-$days days");
        $h_diff = $today->diff($end);
        $hours = $h_diff->h;
        // Minutes
        $end = $end->modify("-$hours hours");
        $i_diff = $today->diff($end);
        $minutes = $i_diff->i;
        // Seconds
        $end = $end->modify("-$minutes minutes");
        $s_diff = $today->diff($end);
        $secs = $s_diff->s;

        if ($months > 0 || $years > 0) :
            $this->_color_class = 'green';
        elseif (0 == $months) :
            $this->_color_class = 'yellow';
        endif;

        $expired = $def_today->diff($def_end)->invert;
        if ($expired) :
            $this->_color_class = 'red';
        endif;

        $this->_diff = array(
            'years'   => $years,
            'months'  => $months,
            'weeks'   => $weeks,
            'days'    => $days,
            'hours'   => $hours,
            'minutes' => $minutes,
            'secs'    => $secs,
        );
    }


    /**
     * Disable updates on WP
     */
    public function disable_wp_updates()
    {
        include $this->m_dir . 'includes/disable-updates.php';
    }

    /**
     * Add mailing action
     */
    public function add_mailing_function()
    {
        include $this->m_dir . 'includes/mailing-function.php';
    }

    /**
     * Create cronjob on timer end
     */
    public function create_reminder()
    {
        wp_clear_scheduled_hook('cab_reminder_action');
        wp_schedule_single_event(strtotime($this->_date), 'cab_reminder_action');
    }

    /**
     * Create cronjob on timer end
     */
    public function dash_endpoint()
    {
        include $this->m_dir . 'includes/dash-endpoint.php';
    }
}

new Cab_M_Timer();
