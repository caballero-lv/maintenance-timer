<div class="wrap">
    <h2><?php echo __('Maintenance timer options'); ?></h2>

    <?php if(defined('CAB_M_TIMER_SAVED') && CAB_M_TIMER_SAVED == true ) : ?>

        <div id="message" class="updated notice notice-success is-dismissible below-h2">
            <p><?php echo __('Settings updated.', 'cab-timer') ?></p>
            <button type="button" class="notice-dismiss">
                <span class="screen-reader-text"><?php echo __('Dismiss this notice.', 'cab-timer'); ?></span>
            </button>
        </div>

    <?php endif; ?>

    <form method="POST" action="" autocomplete="off">
        <input autocomplete="false" name="hidden" type="text" style="display:none;">

        <?php wp_nonce_field('cab-save-m-timer-options', 'cab_m_timer_nonce'); ?>

        <table class="form-table">
            <tbody>

                <tr>
                    <th scope="row">
                        <label for="cab-m-timer-date"><?php echo __('Maintenance date'); ?></label>
                    </th>
                    <td>
                        <input class="regular-text cab-m-date" id="cab-m-timer-date" name="cab-m-timer-date" type="text" value="<?php echo $this->_date; ?>">
                    </td>
                </tr>

                <tr>
                    <th scope="row"><label for="cab-m-timer-date-last"><?php echo __('Last update date'); ?></label></th>
                    <td>
                        <input class="regular-text cab-m-date" id="cab-m-timer-date-last" name="cab-m-timer-date-last" type="text" value="<?php echo $this->_last_date; ?>">
                    </td>
                </tr>

                <tr>
                    <th scope="row"><label for="cab-m-timer-pers"><?php echo __('Manager'); ?></label></th>
                    <td>
                        <select name="cab-m-timer-pers" id="cab-m-timer-pers">
                            <option value="">Please select</option>
                            <?php foreach( $this->m_pers as $id => $pers ): ?>
                                <option value="<?php echo $id; ?>" <?php if($id == $this->_man) {echo 'selected';
                                               } ?>>
                                <?php echo $pers['name'] . ' ' . $pers['lastname']; ?>
                                </option>
                            <?php endforeach; ?>
                        </select>
                    </td>
                </tr>

                <tr>
                    <th scope="row"></th>
                    <td><input name="cab-m-timer-submit" type="submit" id="cab-m-timer-submit" value="<?php echo __('Save') ?>" class="button button-primary"></td>
                </tr>

            </tbody>
        </table>
    </form>
</div>
