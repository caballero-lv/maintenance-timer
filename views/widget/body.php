<?php
$pers = $this->_man ? $this->m_pers[ $this->_man ] : null; ?>
<div class="cab-maint cab-maint--<?php echo $this->_color_class; ?>">
	<div class="cab-maint__header">
		<div class="cab-maint__header__image">
			<img width="100" height="82"
				src="<?php echo $this->m_uri; ?>/app/images/tools-<?php echo $this->_color_class; ?>.png">
		</div>
		<?php
		if ( $this->_color_class != 'red' ) :
			$title = __( 'Nākamā mājas lapas apkope jāveic pēc:' );
		else :
			$title = __( 'Jums steidzami ir jāatjaunina lapa!' );
		endif;
		?>
		<p class="cab-maint__header__text" data-container="default"><?php echo wp_kses_post( $title ); ?></p>

		<?php
		if ( $pers ) :
			?>
			<p class="cab-maint__header__text cab-maint__person hidden"><?php echo wp_kses_post( $pers['prefix'] . ': ' . $pers['name'] . ' ' . $pers['lastname'] ); ?></p>
			<?php
		endif;
		?>
	</div>

	<?php
	if ( $this->_color_class != 'red' ) :
		include $this->m_dir . 'views/widget/counter.php';
	else :
		include $this->m_dir . 'views/widget/update.php';
	endif;
	?>

	<div class="cab-maint__box" data-container="contacts">
		<a href="#" class="cab-maint__box__close cab-maint__toggle" data-target="default">
			<svg xmlns="http://www.w3.org/2000/svg" width="22" height="22"><path fill-rule="evenodd" clip-rule="evenodd" d="M11.042 0C4.944 0 0 4.944 0 11.042c0 6.099 4.944 11.042 11.042 11.042 6.099 0 11.042-4.943 11.042-11.042C22.084 4.944 17.141 0 11.042 0zm4.497 14.081l-1.455 1.455-2.969-2.969-2.753 2.753-1.414-1.414 2.753-2.753-3.156-3.156L8 6.542l3.156 3.156 3.122-3.122 1.414 1.415-3.122 3.122 2.969 2.968z"/></svg>
		</a>

		<div class="clearfix">
			<div class="col7 last">
				<ul>
					<?php
					if ( $pers ) :
						?>
						<li class="cab-maint__pers cab-maint__pers--phone"><?php echo $pers['phone']; ?></li>
						<li class="cab-maint__pers cab-maint__pers--email">
						<?php
						if ( '1' === $this->_man && isset( $pers['email_pragmatic'] ) ) :
							?>
							<a href="mailto:<?php echo $pers['email_pragmatic']; ?>" target="_blank"><?php echo $pers['email_pragmatic']; ?></a></li>
							<?php
						else :
							?>
							<a href="mailto:<?php echo $pers['email']; ?>" target="_blank"><?php echo $pers['email']; ?></a></li>
							<?php
						endif;
						?>
						<li class="cab-maint__pers cab-maint__pers--web">
							<?php
							$website = '1' === $this->_man ? 'pragmatik' : 'caballero';
							?>
							<a href="<?php echo esc_url( 'https://www.' . $website . '.lv' ); ?>" target="_blank"><?php echo wp_kses_post( 'www.' . $website . '.lv' ); ?></a>
						</li>
						<?php
					endif;
					?>
				</ul>
			</div>
		</div>
	</div>

	<div class="cab-maint__box" data-container="info">
		<a href="#" class="cab-maint__box__close cab-maint__toggle" data-target="default">
			<svg xmlns="http://www.w3.org/2000/svg" width="22" height="22"><path fill-rule="evenodd" clip-rule="evenodd" d="M11.042 0C4.944 0 0 4.944 0 11.042c0 6.099 4.944 11.042 11.042 11.042 6.099 0 11.042-4.943 11.042-11.042C22.084 4.944 17.141 0 11.042 0zm4.497 14.081l-1.455 1.455-2.969-2.969-2.753 2.753-1.414-1.414 2.753-2.753-3.156-3.156L8 6.542l3.156 3.156 3.122-3.122 1.414 1.415-3.122 3.122 2.969 2.968z"/></svg>
		</a>
		<p class="cab-maint__textmain"><?php echo __( 'Ir 3 galvenie iemesli, kāpēc ir jāatjauno WordPress (WP) lapa:' ); ?></p>
		<ol>
			<li>
				<p><?php echo __( 'Drošība - WordPress ir viena no populārākajām un lielākajām mājas lapu izstrādes platformām pasaulē, līdz ar to ir arī daudz tādu cilvēku, kas mēģina šo platformu uzlauzt, iegūt datus no mājas lapām vai citādi traucēt lapu un platformas darbību. Tāpēc WP nepārtraukti strādā pie tā, lai novērstu "drošības caurumus" savā sistēmā, šie uzlabojumi ir pieejami vienīgi veicot atjauninājumu.' ); ?></p>
			</li>
			<li>
				<p><?php echo __( 'Ātrdarbība - ātrdarbība ir atkarīga no ļoti daudziem faktoriem, piemēram, bilžu izmēra, hostinga provaidera, servera konfigurācijas, kā arī programmatūras versijas. Lai būtu droši, ka lapa nav lēna tādēļ, ka WP ir novecojis, to ir nepieciešams regulāri atjaunināt.' ); ?></p>
			</li>
			<li>
				<p><?php echo __( 'Platformas kļūdu labojumi - lai arī cik liela un laba būtu WP platforma, arī tajā ir atrodamas kļūdas vai vienkārši lietas, kas nestrādā. WP komanda  nodrošina šos kļūdu labojumus piedāvājot WP atjauninājumus, kurus ir nepieciešams uzstadīt arī Jūsu lapai, lai nodrošinātu projekta stabilitāti un nekļūdīgu darbību.' ); ?></p>
			</li>
		</ol>
	</div>


	<div class="cab-maint__info">
		<?php
		if ( $pers ) :
			?>
			<a href="#" class="cab-maint__btn cab-maint__btn--dark cab-maint__toggle" data-target="contacts"><?php echo __( 'Kontakti' ); ?></a>
			<?php
		endif;
		?>
		<a href="#" class="cab-maint__btn cab-maint__btn--light cab-maint__toggle" data-target="info"><?php echo __( 'Kāpēc nepieciešams atjaunot lapu?' ); ?></a>
	</div>

	<div class="cab-maint__footer">
		<?php
		if ( $pers ) :
			if ( '1' === $this->_man ) :
				?>
				<a href="http://www.pragmatik.lv/" target="_blank">
					<img src="<?php echo $this->m_uri; ?>/app/images/pragmatic.png">
				</a>
				<?php
			else :
				?>
				<a href="http://www.caballero.lv/" target="_blank">
					<img width="70" height="30"
						src="<?php echo $this->m_uri; ?>/app/images/caballero.png">
				</a>
				<?php
			endif;
		endif;
		?>
	</div>
</div>
