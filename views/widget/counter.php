<div class="cab-maint__box cab-maint__box--open" data-container="default">
	<ul class="cab-maint__timer">
		<?php
		$count = 0;

		if ( $this->_diff['years'] > 0 ) : ?>
			<li class="cab-maint__timer__col">
				<p id="cab-timer-years" class="cab-maint__num">&nbsp;</p>
				<p><?php echo __( 'gadiem' ); ?></p>
			</li>
			<?php
			$count++;
		endif;

		if ( $this->_diff['months'] > 0 ) : ?>
			<li class="cab-maint__timer__col">
				<p id="cab-timer-months" class="cab-maint__num">&nbsp;</p>
				<p><?php echo __('mēnešiem'); ?></p>
			</li>
			<?php
			$count++;
		endif;

		/*
		if ( $this->_diff['weeks'] > 0 ) : ?>
			<li class="cab-maint__timer__col">
				<p id="cab-timer-weeks" class="cab-maint__num">&nbsp;</p>
				<p><?php echo __('nedēļām'); ?></p>
			</li>
			<?php
			$count++;
		endif;
		*/

		if ( $count < 4 ) :
			?>
			<li class="cab-maint__timer__col">
				<p id="cab-timer-days" class="cab-maint__num">&nbsp;</p>
				<p><?php echo __('dienām'); ?></p>
			</li>
			<?php
			$count++;
		endif;

		if ( $count < 4 ) :
			?>
			<li class="cab-maint__timer__col">
				<p id="cab-timer-hours" class="cab-maint__num">&nbsp;</p>
				<p><?php echo __('stundām'); ?></p>
			</li>
			<?php
			$count++;
		endif;

		if ( $count < 4 ) :
			?>
			<li class="cab-maint__timer__col">
				<p id="cab-timer-minutes" class="cab-maint__num">&nbsp;</p>
				<p><?php echo __('minūtēm'); ?></p>
			</li>
			<?php
			$count++;
		endif;

		if ( $count < 4 ) :
			?>
			<li class="cab-maint__timer__col">
				<p id="cab-timer-seconds" class="cab-maint__num">&nbsp;</p>
				<p><?php echo __('sekundēm'); ?></p>
			</li>
			<?php
			$count++;
		endif;
		?>
	</ul>
</div>
