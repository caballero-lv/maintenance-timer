<div class="cab-maint__box cab-maint__box--open" data-container="default">
	<p class="cab-maint__update"><?php echo __('Lapas drošības apdraudējums!'); ?></p>
	<p class="cab-maint__update--small"><?php echo __('Lapa pēdējo reizi atjauniāta:') . ' ' . $this->_last_date; ?></p>
</div>
