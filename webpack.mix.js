let mix = require("laravel-mix");

// Genetic
mix.setPublicPath("app");

// JS
mix.js("assets/js/cab-m-timer.js", "js");

// CSS
mix.sass(`assets/sass/screen.scss`, "styles").options({
  processCssUrls: false,
  postCss: [require("autoprefixer")],
});

// Extra
if (mix.inProduction()) {
  mix.sourceMaps().version();
}

mix.disableNotifications();
